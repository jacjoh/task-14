﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MovieTitle = table.Column<string>(type: "nvarchar(70)", maxLength: 70, nullable: true),
                    Genre = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ReleaseYear = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Genre", "MovieTitle", "ReleaseYear" },
                values: new object[,]
                {
                    { 1, "Action,Superhero,Science Fiction,Fantasy", "Captain Marvel", 2019 },
                    { 2, "Action,Superhero,Science Fiction,Fantasy,Comedy,Teen", "Spider-Man: Far From Home", 2019 },
                    { 3, "Action,Superhero,Science Fiction,Fantasy,Comedy,Teen", "Black Panther", 2018 },
                    { 4, "Action,Superhero,Science Fiction,Fantasy,Comedy,Teen", "Ant-Man and the Wasp", 2018 },
                    { 5, "Action,Superhero,Science Fiction,Fantasy,Comedy,Teen", "Doctor Strange", 2016 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Movies");
        }
    }
}
