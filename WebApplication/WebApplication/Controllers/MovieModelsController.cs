﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesAPI.DTOs.Movie;
using MoviesAPI.Models;
using WebApplication.DTOs;

namespace WebApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieModelsController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public MovieModelsController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all movies stored
        /// </summary>
        /// <returns>a list with movie objects</returns>
        // GET: api/MovieModels
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieDto>>> GetMovies()
        {
            var movies = await _context.Movies.ToListAsync();
            List<MovieDto> moviedto = _mapper.Map<List<MovieDto>>(movies); 
            return moviedto;
        }

        /// <summary>
        /// Gets a movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns>a movie object</returns>
        // GET: api/MovieModels/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDto>> GetMovieModel(int id)
        {
            var movie = await _context.Movies.FindAsync(id);

            if (movie== null)
            {
                return NotFound();
            }

            MovieDto moviedto = _mapper.Map<MovieDto>(movie);

            return moviedto;
        }

        // PUT: api/MovieModels/5
        /// <summary>
        /// Updates a movie entity
        /// </summary>
        /// <param name="id"></param>
        /// <param name="moviedto"></param>
        /// <returns>no content</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovieModel(int id, MoviePostDto moviedto)
        {
            MovieModel movieModel = _mapper.Map<MovieModel>(moviedto);

            movieModel.Id = id;

            _context.Entry(movieModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MovieModels
        /// <summary>
        /// Adds a new movie entry
        /// </summary>
        /// <param name="moviedto"></param>
        /// <returns>the created movie object</returns>
        [HttpPost]
        public async Task<ActionResult<MovieDto>> PostMovieModel(MoviePostDto moviedto)
        {
            MovieModel movieModel = _mapper.Map<MovieModel>(moviedto);

            _context.Movies.Add(movieModel);
            await _context.SaveChangesAsync();

            var createddto = _mapper.Map<MovieDto>(movieModel);

            return CreatedAtAction("GetMovieModel", new { id = movieModel.Id }, createddto);
        }

        // DELETE: api/MovieModels/5
        /// <summary>
        /// Deletes a movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns>the deleted movie objects</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<MoviePostDto>> DeleteMovieModel(int id)
        {
            var movieModel = await _context.Movies.FindAsync(id);
            if (movieModel == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movieModel);
            await _context.SaveChangesAsync();

            MoviePostDto moviedto = _mapper.Map<MoviePostDto>(movieModel); 

            return moviedto;
        }

        private bool MovieModelExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
    }
}
