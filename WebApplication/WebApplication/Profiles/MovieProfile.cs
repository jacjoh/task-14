﻿using AutoMapper;
using MoviesAPI.DTOs.Movie;
using MoviesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.DTOs;

namespace MoviesAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<MovieModel, MovieDto>().ReverseMap();
            CreateMap<MovieModel, MoviePostDto>().ReverseMap();
        }
    }
}
