﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Models
{
    public class MovieDbContext : DbContext
    {
        public DbSet<MovieModel> Movies { get; set; }
        public MovieDbContext(DbContextOptions<MovieDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        //Adds data to the created db
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MovieModel>().HasData(new MovieModel() {
                Id = 1,
                MovieTitle = "Captain Marvel",
                Genre = "Action,Superhero,Science Fiction,Fantasy",
                ReleaseYear = 2019,
            });
            modelBuilder.Entity<MovieModel>().HasData(new MovieModel()
            {
                Id = 2,
                MovieTitle = "Spider-Man: Far From Home",
                Genre = "Action,Superhero,Science Fiction,Fantasy,Comedy,Teen",
                ReleaseYear = 2019,
            });
            modelBuilder.Entity<MovieModel>().HasData(new MovieModel()
            {
                Id = 3,
                MovieTitle = "Black Panther",
                Genre = "Action,Superhero,Science Fiction,Fantasy,Comedy,Teen",
                ReleaseYear = 2018,
            });
            modelBuilder.Entity<MovieModel>().HasData(new MovieModel()
            {
                Id = 4,
                MovieTitle = "Ant-Man and the Wasp",
                Genre = "Action,Superhero,Science Fiction,Fantasy,Comedy,Teen",
                ReleaseYear = 2018,
            });
            modelBuilder.Entity<MovieModel>().HasData(new MovieModel()
            {
                Id = 5,
                MovieTitle = "Doctor Strange",
                Genre = "Action,Superhero,Science Fiction,Fantasy,Comedy,Teen",
                ReleaseYear = 2016,
            });
        }
    }
}
