# task-14

Simple API for movies, where one can store, update, delete and get movies. The movies are stored in a mssql database. The web api is created with EF.

There is created a dockerfile to replicate the api. docker-compose have a container of SQL server in the container. The image is then placed on dockerhub. The link to the dockerhub is https://hub.docker.com/r/jacobjohnsen/movieapi